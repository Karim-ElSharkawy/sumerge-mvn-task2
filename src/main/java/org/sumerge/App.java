package org.sumerge;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;

public class App {
    private static final Logger logger = LogManager.getLogger(App.class);

    public static void main(String[] args) {
        // Task 1
        ResourceBundle resourceBundle = ResourceBundle.getBundle("Profile");
        BasicConfigurator.configure();
        logger.info("Hello world Main with profile: " + resourceBundle.getString("profileKeyWord"));

        // Task 2
        List<Employee> employees = Arrays.asList(
                new Employee("Mohamed Elsharkawy", "Senior Software Engineer", "01118321812"),
                new Employee("Karim Elsharkawy", "Associate Software Engineer", "01118321812"),
                new Employee("Khaled Ashraf", "Associate Software Engineer", "01123361514"),
                new Employee("Anas Saleh", "Associate Software Engineer", "02312051524"),
                new Employee("Mohamed Gamal", "Associate Software Engineer", "01151235165"),
                new Employee("Ahmed Raafat", "Associate Software Engineer", "01151235165"),
                new Employee("Ahmed Hatem", "Software Engineer", "01241515161"),
                new Employee("Hussain Yahya", "Software Engineer", "0123124151"),
                new Employee("Mohamed Kadry", "Software Engineer", "0151521421"),
                new Employee("Ahmed Anwar", "Software Engineer", "01159634321")
        );
        // Show only group with 2 or more employees.
        //Map<String, List<Employee>> result = employees.stream().collect(Collectors.groupingBy(Employee::getRole)).entrySet().stream().filter(entry -> entry.getValue().size() > 1).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        // Show all groups
        Map<String, List<Employee>> result = employees.stream().collect(Collectors.groupingBy(Employee::getRole));

        result.entrySet().forEach(entry -> {
            if (entry.getValue().size() < 2)
                try {
                    throw new Exception("Title with Count Less than 2");
                } catch (Exception e) {
                    System.out.println("--EXCEPTION--");
                    System.out.println(e);
                    System.out.println("--EXCEPTION--");
                    return;
                }

            System.out.println("Title: " + entry.getKey() + ", COUNT " + entry.getValue().size());
            System.out.println("----|");
            entry.getValue().forEach(entryItems -> System.out.println("Name: " + entryItems.getName() + ", Telephone: " + entryItems.getTelephone()));
            System.out.println("--- # --- # --- # --- # --- # --- # --- # --- # ---" + "\n");
        });
    }
}
